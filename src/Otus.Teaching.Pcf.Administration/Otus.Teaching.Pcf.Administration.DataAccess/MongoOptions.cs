﻿namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoOptions
    {
        public const string SectionName = "MongoDb";
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}