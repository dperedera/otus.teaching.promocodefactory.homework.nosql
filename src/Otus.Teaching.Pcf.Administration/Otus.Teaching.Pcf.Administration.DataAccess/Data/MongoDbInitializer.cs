﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IOptions<MongoOptions> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _database = client.GetDatabase(options.Value.DatabaseName);
        }

        public async void InitializeDb()
        {
            var roles = await RecreateCollectionAsync<Role>();
            var employees = await RecreateCollectionAsync<Employee>();

            await roles.InsertManyAsync(FakeDataFactory.Roles);
            await employees.InsertManyAsync(FakeDataFactory.Employees);
        }
        
        private async Task<IMongoCollection<TEntity>> RecreateCollectionAsync<TEntity>()
            where TEntity : BaseEntity
        {
            var collectionName = $"{typeof(TEntity)}s";
            await _database.DropCollectionAsync(collectionName);
            await _database.CreateCollectionAsync(collectionName);

            return _database.GetCollection<TEntity>(collectionName);
        }
    }
}